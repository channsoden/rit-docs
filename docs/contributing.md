---
title: Community Contributions Guide
keywords: contribution, contributing
tags: [docs]
---

# Community Contributions Guide

The RIT Documentation website is hosted on [GitLab
Pages](https://pages.gitlab.io) and uses [mkdocs](http://mkdocs.org) plus the
[Material theme](https://squidfunk.github.io/mkdocs-material/getting-started/).
You can [browse its source code](https://gitlab.com/ucb-rit/rit-docs), fork it,
and create a merge request if you'd like to contribute some changes.

!!! note 
    For RIT staff, please see our internal docs to learn how to
    make changes to the site.

Below we provide some brief tips for working with mkdocs. For full documentation
visit [mkdocs.org](http://mkdocs.org).

## `mkdocs` tips

### Commands

* `mkdocs new [dir-name]` - Create a new project.
* `mkdocs serve` - Start the live-reloading docs server.
* `mkdocs build` - Build the documentation site.
* `mkdocs help` - Print this help message.

### Project layout

    mkdocs.yml    # The configuration file.
    docs/
        index.md  # The documentation homepage.
        ...       # Other markdown pages, images and other files.

### Hyperlinks

All internal links (meaning they point to somewhere on the same domain) in an `mkdocs` site must be _relative_. This means that when adding the link you should use `..` to mean go back one directory. For example, if you're working on the page at `docs/services/high-performance-computing/user-guide/storing-data.md` and you want to link to the page at `docs/services/high-performance-computing/getting-help/index.md`, you can write the link as:

```
<a href="../../getting-help/">Getting Help</a>
```

Note that the first `..` corresponds to the directory `docs/services/high-performance-computing/user-guide/`, and the second takes us back one more level to `docs/services/high-performance-computing/`. If we were creating a link in the `index.md` file of the `user-guide/` directory instead of the `storing-data.md` file, then `..` would instead correspond to `docs/services/high-performance-computing/` rather than `docs/services/high-performance-computing/user-guide/`.

### Q&A Example

??? question "Q. What is the meaning of life, the universe, and everything?"
    
    42

To achieve the above style, the syntax is:
```
??? question "Q. What is the meaning of life, the universe, and everything?"
    
    42
```
The question is in quotes following `??? question` and the answer goes below the question. Note that the answer must be indented using 4 or more spaces, and all lines until the first non-indented line will be included in the answer box.
