---
title: Getting Help
keywords: high performance computing
tags: [hpc, help, consulting]
---

## User Support

#### Ticketing System

To get help on any topic - such as to ask questions about the BRC clusters (Savio and Vector), report a problem, or provide suggestions - please email us at <a href="mailto:brc-hpc-help@berkeley.edu">brc-hpc-help@berkeley.edu</a>. Doing so will create an issue ticket, and you'll receive email responses whenever the ticket's status changes.

Support is provided during regular business hours (Monday through Friday, from 8:00 am to 5:00 pm Pacific Time). Support during off-hours, weekends, and holidays is Best-Effort, based on availability of staff and criticality of the problem.

## Consulting

Research IT offers consulting for UC Berkeley researchers. From identifying the best computing solution to working with sensitive data, Research IT consultants are poised at the intersection of research and technology and can help. We are here to help understand your needs, match you to appropriate resources, and help you get started using them. Our diverse team is made up of experts in both data and computing from a wide variety of domains. 

For consultation, please email <a href="mailto:research-it-consulting@berkeley.edu">research-it-consulting@berkeley.edu</a>.

#### Virtual Office Hours

Research IT offers virtual office hours for in-person support. For times and location, please see <a href="https://research-it.berkeley.edu/consulting" target="_blank">here</a>.

No appointment is necessary. 

## Status and Announcements

Please also see the [Status and Announcements](http://research-it.berkeley.edu/services/high-performance-computing/status-and-announcements) page for listings of scheduled downtimes for the cluster, general system status announcements, and a graphical display of live utilization and status information for many of the cluster's compute nodes.
