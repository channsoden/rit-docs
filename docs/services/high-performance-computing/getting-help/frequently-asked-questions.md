---
title: Frequently Asked Questions
keywords: high performance computing
tags: [hpc]
---

# Frequently Asked Questions

## General FAQs

??? question "Q. How can I access the Faculty Computing Allowance application (Requirements Survey) and Additional User Request forms? I'm seeing a "You need permission" error."
    <span id="q-how-can-i-access-the-faculty-computing-allowance-application-requirements-survey-and-additional-user-request-forms-i-m-seeing-a-you-need-permission-error-"></span>
    <p>A. You&nbsp;need to <a href="https://calnetweb.berkeley.edu/calnet-me" target="_blank">authenticate via CalNet</a> to access the online forms for applying for a Faculty Computing Allowance, and for requesting additional user accounts on the Savio cluster.</p>
    <p>When accessing either form, you may encounter the error message, "You need permission. This form can only be viewed by users in the owner's organization", under either of these circumstances:</p>
    <p><span colspan="2"><span style="word-wrap:break-word;display:block">1. If you haven't already successfully logged in via CalNet. (If you don't have a CalNet ID, please work with a UCB faculty member or other researcher who can access the form on your behalf.)</span></span><br>
    2. If you've logged in via CalNet, but you're also simultaneously connected, in your browser, to a non-UCB Google account; for instance, to access a personal Gmail account. (If so, the easiest way to access the online forms might be to use a private/incognito window in your primary browser, or else use a second browser on your computer, one in which you aren't already logged into a Google account. As an alternative, you can first log out of all of your Google accounts in your primary browser, before attempting to access these forms.)</p>

??? question "Q. How do I know which partition, account, and QoS I should use when submitting my job?"
    <span id="q-how-do-i-know-which-partition-account-and-qos-i-should-use-when-submitting-my-job-"></span>
    <p style="font-size: 13.008px;"><strong>A.</strong>&nbsp;SLURM provides a command you can run to check on the partitions, accounts and Quality of Service (QoS) options that you're permitted to use. Please run the "<strong>sacctmgr show associations user=$USER</strong>" command to find this information for your job submission. You can also add the "-p" option to this command to get a parsable output, i.e., "<strong>sacctmgr -p show associations user=$USER</strong>".</p>

??? question "Q. How can I check on my Faculty Computing Allowance (FCA) usage?"
    <span id="q-how-can-i-check-on-my-faculty-computing-allowance-fca-usage-"></span>
    <p style="font-size: 13.008px;"><strong>A.</strong>&nbsp;Savio provides a "<strong>check_usage.sh</strong>" command line tool you can use to check cluster usage by user or account.</p>
    <p style="font-size: 13.008px;">Running "<strong>check_usage.sh -E"</strong>&nbsp;will report total usage by the current user, as well as a breakdown of their usage within each of their related project accounts, since the most recent reset/introduction date (normally June 1st of each year). To check usage for another user on the system, add a "<strong>-u sampleusername</strong>" option (substituting an actual user name for 'sampleusername' in this example).</p>
    <p style="font-size: 13.008px;">You can check usage for a project's account, rather than for an individual user's account, with the '<strong>-a sampleprojectname</strong>' option to this command (substituting an actual account name for 'sampleprojectname' in this example).</p>
    <p style="font-size: 13.008px;">Also, when checking usage for either users or accounts, you can display usage during a specified time period by adding start date (-s) and/or end date (-e) options, as in "<strong>-s YYYY-MM-DD</strong>" and "<strong>-e YYYY-MM-DD</strong>" (substituting actual Year-Month-Day values for 'YYYY-MM-DD' in these examples). Run "<strong>check_usage.sh -h</strong>" for more information and additional options.</p>
    <p style="font-size: 13.008px;">When checking usage for accounts that have overall usage limits (such as Faculty Computing Allowances), the value of the&nbsp;<a href="../../../../services/high-performance-computing/user-guide/running-your-jobs/service-units-savio">Service Units (SUs)</a>&nbsp;field is color-coded to help you see at a glance how much computational time is still available: green means your project has used less than 50% of its available SUs; yellow means your project has used more than 50% but less than 100% of its available SUs; and red means your project has used 100% or more of its available SUs (and has likely been disabled). Note that if you specify the starttime and/or endtime with "-s" and/or "-e" option(s) you will not get the color coded output.</p>
    <p style="font-size: 13.008px;">A couple of output samples from running this command line tool with user and project options, respectively, along with some tips on interpreting that output:</p>
    <p style="font-size: 13.008px;"><code>$&nbsp;<strong>check_usage.sh -E -u sampleusername</strong></code>&nbsp;<code>Usage for USER sampleusername [2016-06-01T00:00:00, 2016-08-17T18:18:37]: 38 jobs,<br>
    1311.40 CPUHrs, 1208.16 SUs used<br>
    Usage for USER sampleusername in ACCOUNT co_samplecondoname [2016-06-01T00:00:00,</code>&nbsp;<code>2016-08-17T18:18:37]: 23 jobs, 857.72 CPUHrs, 827.59 SUs<br>
    Usage for USER sampleusername in ACCOUNT fc_sampleprojectname [2016-06-01T00:00:00,<br>
    2016-08-17T18:18:37]: 15 jobs, 453.68 CPUHrs, 380.57 SUs</code></p>
    <p style="font-size: 13.008px;">Total usage from June 1, 2016 through the early evening of August 17, 2016 by the 'sampleusername' cluster user consists of 38 jobs run, using approximately 1,311 CPU hours, and resulting in usage of approximately 1208 Service Units. (The total number of&nbsp;<a href="../../../../services/high-performance-computing/user-guide/running-your-jobs/service-units-savio">Service Units</a>&nbsp;is less than the total number of CPU hours in this example, because some jobs were run on older or otherwise less expensive hardware pools (partitions) which cost less than one Service Unit per CPU hour.)</p>
    <p style="font-size: 13.008px;">Of that total usage, 23 jobs were run under the Condo project account 'co_samplecondoname', using approximately 858 CPU hours and 828 Service Units, and 15 jobs were run under the Faculty Computing Allowance project account 'fc_sampleprojectname', using approximately 454 CPU hours and 381 Service Units.</p>
    <p style="font-size: 13.008px;"><code>$&nbsp;<strong>check_usage.sh -a fc_sampleprojectname</strong><br>
    Usage for ACCOUNT fc_sampleprojectname [2016-06-01T00:00:00, 2016-08-17T18:19:15]: 156<br>
    jobs, 85263.80 CPUHrs,&nbsp;<span style="color: green;">92852.12</span> SUs used from an allocation of 300000 SUs.</code></p>
    <p style="font-size: 13.008px;">Usage from June 1, 2016 through the early evening of August 17, 2016 by all cluster users of the Faculty Computing Allowance account 'fc_sampleprojectname'&nbsp; consists of 156 jobs run, using a total of approximately 85,263 CPU hours, and resulting in usage of approximately 92,852 Service Units. (The total number of&nbsp;<a href="../../../../services/high-performance-computing/user-guide/running-your-jobs/service-units-savio">Service Units</a>&nbsp;is greater than the total number of CPU hours in this example, because some jobs were run on hardware pools (partitions) which cost more than one Service Unit per CPU hour.) The total Faculty Computing Allowance allocation for this project's account is 300,000 Service Units, so there are approximately 207,148 Service Units still available for running jobs during the remainder of the current Allowance year (June 1 to May 31): 300,000 total Service Units granted, less 92,852 used to date. The total of 92,852 Service Units used to date is colored green, because this project's account has used less than 50% of its total Service Units available.</p>
    <p style="font-size: 13.008px;">To also view individual usages by each cluster user of the Faculty Computing Allowance project account 'fc_sampleprojectname', you can add a '-E' option to the above command; e.g.,&nbsp;<code><strong>check_usage.sh -E -a fc_sampleprojectname</strong></code></p>
    <p style="font-size: 13.008px;">Finally, if your Faculty Computing Allowance has become completely&nbsp;<a href="../../../../services/high-performance-computing/getting-account/options-when-fca-exhausted" target="_blank">exhausted</a>, the output from running the "<strong>check_usage.sh</strong>" command line tool will by default show only information for the period of time&nbsp;<em>after</em>&nbsp;your job scheduler account was disabled; for example:</p>
    <p style="font-size: 13.008px;"><code>Usage for ACCOUNT fc_sampleprojectname [2017-04-05T11:00:00, 2017-04-24T17:19:12]: 3 jobs, 0.00 CPUHrs, 0.00 SUs from an allocation of 0 SUs.<br>
    ACCOUNT fc_sampleprojectname has exceeded its allowance. Allocation has been set to 0 SUs.<br>
    Usage for USER sampleusername in ACCOUNT fc_sampleprojectname [2017-04-05T11:00:00, 2017-04-24T17:19:12]: 0 jobs, 0.00 CPUHrs, 0.00 (0%) SUs</code></p>
    <p style="font-size: 13.008px;">To display the - more meaningful - information about the earlier usage that resulted in the Faculty Computing Allowance becoming exhausted, use the start date (-s) option and specify the most recently-passed June 1st&nbsp;-&nbsp;the first day of the current Allowance year&nbsp;-&nbsp;as that start date. E.g., to view usage for an Allowance that became exhausted anytime during the 2018-19&nbsp;Allowance year, use a start date of June 1, 2018:</p>
    <p style="font-size: 13.008px;"><strong><code>check_usage.sh -E -s 2018-06-01 -a fc_sampleprojectname</code></strong></p>

??? question "Q. How can I use $SLURM_JOBID in my output and/or error log filenames?"
    <span id="q-how-can-i-use-slurm-jobid-in-my-output-and-or-error-log-filenames-"></span>
    <p style="font-size: 13.008px;"><strong>A.</strong>&nbsp;SLURM uses a different way to manage SLURM-specific environment variables, which is in turn different than PBS or other job schedulers. Before you use a SLURM environment variable, please check its scope of availability by entering "man sbatch" or "man srun".</p>
    <p style="font-size: 13.008px;">To use the JOBID as part of the output and/or error file name, it takes a filename pattern instead of any of the environment variables. Please "man sbatch" for details. As a quick reference, the proper syntax is "<strong>--output=%j.out</strong>".</p>

??? question "Q. When is my job going to start to run?"
    <span id="q-when-is-my-job-going-to-start-to-run-"></span>
    <p style="font-size: 13.008px;"><strong>A.</strong>&nbsp;In most cases you should be able to get an estimate of when your job is going to start by running the command "<strong>squeue --start -j $your_job_id</strong>" (substituting your actual job ID for '$your_job_id' in this example). However under the following two circumstances you may get a "N/A" as the reported START_TIME.</p>
    <ul style="font-size: 13.008px;"><li>You did not specify the run time limit for the job with "--time" option. This will block the job from being back filled.</li>
    <li>The job that's currently running and blocking your job from starting didn't specify the run time limit with "--time" option.</li>
    </ul><p style="font-size: 13.008px;">Thus, as the best practice to improve the scheduler efficiency and to obtain a more accurate estimate of the start time, it is highly recommended to always use "--time" to specify a run time limit for your jobs. It is also worth noting that the start time is only an estimate based on the current jobs queued in the scheduler. If there are new jobs submitted later with higher priorities this estimate will be updated spontaneously as well. So this estimate should only be used as a reference instead of a guaranteed start time.</p>

??? question "Q. Why is my job not starting to run?"
    <span id="q-why-is-my-job-not-starting-to-run-"></span>
    <p style="font-size: 13.008px;"><strong>A.</strong>&nbsp;Lots of reasons could cause the job to not start at your expected time:</p>
    <ul style="font-size: 13.008px;"><li>The first action that you should take to troubleshoot it is to get an estimate of the start time with "<strong>squeue --start -j $your_job_id</strong>" (substituting your actual job ID for '$your_job_id' in this example). If you are satisfied with the estimated start time you can stop here.</li>
    <li>If you would like to troubleshoot further you can then run "<strong>sinfo -p $partition</strong>" (substituting the actual name of the partition on which you're trying to run your job, such as 'savio2' or 'savio2_gpu', for '$partition' in this example) to see if the resources you are requesting are currently allocated to other jobs or not. As a generality, "idle" nodes are free and available to run new jobs, while nodes in most other statuses are currently unavailable. If you used node features in your job submission file, make sure you only check the resources that match the node feature that you requested. (All the node features are documented on your cluster's webpage.) If the partition on which you want to run your job currently is heavily impacted (has few idle nodes), and you are not satisfied with your job's estimated start time (see above), you might consider running it on another, less-impacted partition to which you also have access, if that partition's features are compatible with your job.</li>
    <li>If you see enough resources available but your job is still not showing a reasonable start time, please run "<strong>sprio</strong>" to see if there are jobs with higher priorities that are currently blocking your job. (For instance, "<strong>sprio -o "%Y %i %u" | sort -n -r</strong>" will show pending jobs across all of Savio's partitions, ordered from highest to lowest priority, together with their job IDs and usernames.)</li>
    <li>If there are no higher priority jobs blocking the resources that you requested, you can check whether there may be any reservation on the resources that you requested with "<strong>scontrol show reservations</strong>". Reservations are used, for instance, to defer the start of jobs whose requested wall clock times might overlap with scheduled maintenance periods on the cluster. In those instances, if your job can be run within a shorter time period, you can adjust its wall clock time to avoid such overlap.</li>
    <li>For Faculty Computing Allowance users, you may also want to check whether your account's allowance has been completely used up, via "<strong>check_usage.sh</strong>". (See the Tip on&nbsp;<a href="#q-how-can-i-check-on-my-faculty-computing-allowance-fca-usage-" class="toc-filter-processed">using this command</a>, above.)</li>
    </ul><p style="font-size: 13.008px;">If, after going through all of these steps, you are still puzzled by why your job is not starting, please feel free to&nbsp;<a href="../../../../services/high-performance-computing/getting-help">contact us</a>.</p>

??? question "Q. How can I check my job's running status?"
    <span id="q-how-can-i-check-my-job-s-running-status-"></span>
    <p style="font-size: 13.008px;"><strong>A.</strong>&nbsp;If you suspect your job is not running properly, or you simply want to understand how much memory or how much CPU the job is actually using on the compute nodes, RIT provides a script "wwall" to check that. "<strong>wwall -j $your_job_id</strong>" provides a snapshot of node status that the job is running on. "<strong>wwall -j $your_job_id -t</strong>" provides a text-based user interface (TUI) to monitor the node status when the job progresses. To exit the TUI, enter "q" to quit out of the interface and be returned to the command line.</p>

??? question "Q. How can I run High-Throughput Computing (HTC) type of jobs? (How can I run multiple jobs on the same node?)"
    <span id="q-how-can-i-run-high-throughput-computing-htc-type-of-jobs-how-can-i-run-multiple-jobs-on-the-same-node-"></span>
    <p style="font-size: 13.008px;"><strong>A.</strong>&nbsp;If you have a set of common tasks that you would like to perform on the cluster, and these tasks share the characteristics of short duration and a decent number of them, they fall into the category of High-Throughput Computing (HTC). Typical applications such as parameter/configuration scanning, divide and conquer approach can all be categorized like this. Resolving an HTC problem isn't easy on a traditional HPC cluster with time and resource limits. However, within the room that one can maneuver, there are still some options available.</p>
    <p style="font-size: 13.008px;">The&nbsp;usage instructions for the "GNU parallel" shell tool can be found at this page:&nbsp;<a href="../../../../services/high-performance-computing/user-guide/running-your-jobs/gnu-parallel">GNU parallel</a></p>
    <p style="font-size: 13.008px;">As well, the Savio cluster also offers&nbsp;<a href="../../../../services/high-performance-computing/system-overview#Compute" class="toc-filter-processed">High Throughput Computing nodes</a>, which may be suitable for some of these types of HTC tasks.</p>

??? question "Q. My job keeps failing with the error "Host key verification failed". How can I resolve this?"
    <span id="q-my-job-keeps-failing-with-the-error-host-key-verification-failed-how-can-i-resolve-this-"></span>
    <p style="font-size: 13.008px;"><strong>A.</strong> The message "Host key verification failed" often means there might be something wrong with your ~/.ssh directory and the files in it. For example, the ~/.ssh directory may not include the 'known_hosts' file. Please try these steps to regenerate your ~/.ssh folder:</p>
    <p><code>$&nbsp;mv ~/.ssh ~/.ssh.orig # move your .ssh folder to a backup</code></p>
    <p><code>$&nbsp;cluster-env</code></p>
    <p style="font-size: 13.008px;">Your ~/.ssh directory should then include, for example, files such as the following:</p>
    <p><code>$ ls -l ~/.ssh<br>
    total 16<br>
    -rw------- 1 myashar ucb 3326 Aug 16  2018 id_rsa<br>
    -rw-r--r-- 1 myashar ucb  747 Aug 16  2018 id_rsa.pub<br>
    -rw------- 1 myashar ucb 3986 Apr  8 00:03 known_hosts</code></p>


??? question "Q. When trying to install Python packages with Conda in my home directory, I receive an error message that includes "Disk quota exceeded" and can’t proceed. How can I resolve this?"
    <span id="q-when-trying-to-install-python-packages-with-conda-i-receive-an-error"></span>
    <p style="font-size: 13.008px;"><strong>A.</strong>&nbsp;When a user is trying to install additional Python packages in their home directory with Conda and/or set up a Conda environment, they may sometimes receive an error message that includes, e.g.,“[Errno 122] Disk quota exceeded” when they've exceeded their home directory 10 GB quota limit. This can happen because Conda installs packages inside the ~/.conda directory (in the user’s home directory) but the user has run out of available storage space there. To work around this, you can move the ~/.conda directory to your scratch directory and then create a symbolic link to it.</p>
    <p style="font-size: 13.008px;">There are a couple ways to do this. If you want to move you existing Conda environment (the cp might take a long time):</p>
    <p><code>mv ~/.conda ~/.conda.orig</code></p>
    <p><code>cp -r ~/.conda.orig /global/scratch/$USER/.conda   # $USER is your Savio username</code></p>
    <p><code>ln -s /global/scratch/$USER/.conda ~/.conda</code></p>
    <p style="font-size: 13.008px;">Once everything is done and working, you can delete your old Conda environment from your home directory to free up space if there are no environments you care about keeping.</p>
    <p><code>rm -rf ~/.conda.orig</code></p>
    <p style="font-size: 13.008px;">You can also replace the "cp -r" line with a mkdir, and then start fresh. This means they'll lose any existing environments, but won't have to wait for the lengthy copy to finish:</p>
    <p><code>mkdir -p /global/scratch/$USER/conda # create a new directory for ~/.conda in scratch</code></p>
    <p style="font-size: 13.008px;">Again, you should keep in mind that the above process will remove any existing conda environments you have, so you might consider exporting these to an environment.yml file if you need to recreate them.</p>
    <p style="font-size: 13.008px;">Also, please keep in mind that you can follow the instructions below to remove redundant/unused Conda environments as needed:</p>
    <p><code>1) conda info --envs (this lists all your environments)</code></p>
    <p><code>2) conda remove --name myenv all (where myenv is the name of the environment you want to remove)</code></p>
    <p style="font-size: 13.008px;">Another option to free up space in your home directory is to delete files inside the ~/.local directory (in you home directory), which is where pip installs python packages (for example) by default. It's also possible to install into someplace other than .local, such as scratch. If there are python packages in ~/.local that are taking up a lot of space, it's cleaner to remove them with pip rather than just deleting files there. Otherwise, you might have issues with Python thinking a package is installed but it has actually been deleted. You can use "pip uninstall $PACKAGE_NAME" for this.</p>
    <p style="font-size: 13.008px;">You can also check if there are files in ~/.conda/pkgs that are taking up a lot of space. If you run</p>
    <p><code>du -h -d 1 ~</code></p>
    <p style="font-size: 13.008px;">you'll see how much space is used by each top-level subdirectory in your home directory (which is what the ~ indicates).</p>
    

??? question "Q. I’m unable to list available software modules and/or I can’t display various directories to navigate to. How can I resolve this?"
    <span id="unable-to-list-available-software-modules"></span>
    <p style="font-size: 13.008px;"><strong>A.</strong>&nbsp;If you find, after logging onto the cluster, that you can’t list available software modules (when using the “module avail” command) and/or you can’t get a listing of directories to navigate to, or if your linux shell prompt looks like, e.g, “-bash-4.2$” instead of, say, “[myashar@ln001 ~]$”, this may indicate that there is something wrong with your shell environment, which in turn may point to a problem with your ~/.bashrc and/or ~/.bash_profile file. If this is the case, then you can try to fix these files manually, or you can replace/switch them with the system default ones using the following commands:</p>
    <p><code>mv ~/.bashrc ~/.bashrc.orig</code></p>
    <p><code>cp /etc/skel/.bashrc ~/.bashrc</code></p>
    <p><code>mv ~/.bash_profile ~/.bash_profile.orig</code></p>
    <p><code>cp /etc/skel/.bash_profile ~/.bash_profile</code></p>
    <p style="font-size: 13.008px;">Then, log out and log back in again, and check whether this has resolved the issue.</p>


??? question "Q. What does the #error "This Intel &lt;math.h&gt; is for use with only the Intel compilers!" mean?"
    <span id="q-what-does-the-error-this-intel-lt-math-h-gt-is-for-use-with-only-the-intel-compilers-mean-"></span>
    <p style="font-size: 13.008px;"><strong>A.</strong>&nbsp;You likely are seeing this error because you have an Intel compiler module loaded in your environment, but you are trying to build your application with a GCC compiler. Please unload any Intel compiler module(s) from your current environment and rebuild with GCC. (See&nbsp;<a href="../../../../services/high-performance-computing/user-guide/accessing-software#accessing-software-using-environment-modules">Accessing Software</a>&nbsp;for instructions on unloading modules.)</p>


??? question "Q. My code uses C++11 features. Do any compilers on the cluster support that?"
    <span id="q-my-code-uses-c-11-features-do-any-compilers-on-the-cluster-support-that-"></span>
    <p style="font-size: 13.008px;"><strong>A.</strong>&nbsp;C++11 (formerly known as C++0x) features have been partially supported by Intel's C++ compilers, beginning with version 11.x, and are fully supported in the 2015.x series. For more details please refer to&nbsp;<a href="https://software.intel.com/en-us/articles/c0x-features-supported-by-intel-c-compiler" target="_blank">Intel's C++11 features support page</a>. Note: to support the full set of C++11 features, GCC 4.8 and above is also needed. Please follow this guidance when compiling your C++ code with C++11 features on the cluster:</p>
    <ul style="font-size: 13.008px;"><li>Start by loading the environment module for the default version of the Intel compilers via&nbsp;<code>module load intel</code>. Compile the code with "<kbd>icpc -std=c++11 some_file</kbd>" (replacing "<kbd>some_file</kbd>" with the actual name of your C++11 source code file).</li>
    <li>If the command above finishes successfully you can stop here. Otherwise please check&nbsp;<a href="https://software.intel.com/en-us/articles/c0x-features-supported-by-intel-c-compiler" target="_blank">Intel's C++11 features support page</a>&nbsp;to learn whether the C++11 features your code uses are supported by the default version of the Intel compilers. If not, please switch to the cluster's environment module that provides a higher version of the Intel compilers. To do so, enter "<kbd>module switch intel intel/xxxx.yy.zz</kbd>" (replacing "<kbd>xxxx.yy.zz</kbd>" with that higher version number; enter "<kbd>module avail</kbd>" to find that number, if needed).</li>
    <li>If your code uses the C++11 Standard Template Library (STL), you’ll also need to load the GCC/4.8.5 software module as a driver; its header files provide support for the C++11 STL. To do so, enter "<kbd>module load gcc/4.8.5</kbd>" before compiling your code.</li>
    </ul>


??? question "Q. How can I see all of the available modules?"
    <span id="q-how-can-i-see-all-of-the-available-modules-"></span>
    <p style="font-size: 13.008px;"><strong>A.</strong> To see an extensive list of all module files, including those only visible after loading other prerequisite modules, enter the following command:</p>
    <p><code>find /global/software/sl-7.x86_64/modfiles -type d -exec ls -d {} \;</code></p>


??? question "Q. Can I get root access to my compute nodes?"
    <span id="q-can-i-get-root-access-to-my-compute-nodes-"></span>
    <p>A. Unfortunately, that is not possible. All the compute nodes download the same operating system image from the master node and load the image into RAM disk, so changes to the operating system on the compute node would not be persistent. If you believe that you may need root access for software installations, or any other purpose related to your research workflow, please <a href="http://research-it.berkeley.edu/contact" target="_blank">contact us</a> and we'll be glad to explore various alternative approaches with you.</p>


??? question "Q. Do you allow users to NFS mount their own storage onto the compute nodes?"
    <span id="q-do-you-allow-users-to-nfs-mount-their-own-storage-onto-the-compute-nodes-"></span>
    <p>A. No. We NFS mount storage across all compute nodes so that data is available independent of which compute nodes are used; however, medium to large clusters can place a very high load on NFS storage servers and many, including Linux-based NFS servers, cannot handle this load and will lock up. A non-responding NFS mount can hang the entire cluster, so we can't risk allowing outside mounts.</p>


??? question "Q. How much am I charged for computational time on Savio?"
    <span id="q-how-much-am-i-charged-for-computational-time-on-savio-"></span>
    <p>A. For those with <a href="../../../../services/high-performance-computing/getting-account/faculty-computing-allowance">Faculty Computing Allowance</a> accounts, usage of computational time on Savio is tracked (in effect, "charged" for, although no costs are incurred) via abstract measurement units called "Service Units." (Please see <a href="../../../../services/high-performance-computing/user-guide/running-your-jobs/service-units-savio">Service Units on Savio</a> for a description of how this usage is calculated.) When all of the Service Units provided under an Allowance have been exhausted, no more jobs can be run under that account. Usage tracking does not impact <a href="../../../../services/high-performance-computing/condos/condo-cluster-service">Condo</a> users, who have no Service Unit-based limits on the use of their associated compute pools.</p>


??? question "Q. How can I acknowledge the Savio&nbsp;Cluster in my presentations or publications?"
    <span id="q-how-can-i-acknowledge-the-savio-nbsp-cluster-in-my-presentations-or-publications-"></span>
    <p>A. You can use the following sentence in order to acknowledge computational and storage services associated with the Savio&nbsp;Cluster:</p>
    <blockquote><p><i>"This research used the Savio&nbsp;computational cluster resource provided by the Berkeley Research Computing program at the University of California, Berkeley (supported by the UC Berkeley Chancellor, Vice Chancellor for Research, and Chief Information Officer)."</i></p></blockquote>
    <p>Acknowledgements of this type are an important factor in helping to justify ongoing funding for, and expansion of, the cluster. As well, we encourage you to <a href="https://docs.google.com/a/berkeley.edu/forms/d/e/1FAIpQLSdqhh2A77-l8N3eOcOzrH508UKfhIvPn8h5gLDUJ9XrRLvA5Q/viewform" target="_blank">tell us how BRC impacts your research</a>&nbsp;(Google Form), at any time!</p>


## Condo Cluster Computing Program FAQs

??? question "Q. What are the benefits of participating in the Condo cluster program?"
    <span id="q-what-are-the-benefits-of-participating-in-the-condo-cluster-program-"></span>
    <p>A. A major incentive for researchers to participate is that they only have to purchase their compute nodes, and support of the compute nodes is provided for free in exchange for their unused compute cycles. In addition to receiving professional systems administration support, researchers will be able to leverage the use of the HPC infrastructure (firewalled subnet, login nodes, commercial compiler, parallel filesystem, etc.) when they use their compute nodes. This infrastructure is provided for free and saves researchers from having to purchase and create any of these components on their own.</p>


??? question "Q. What are the support costs for participating in the Condo program?"
    <span id="q-what-are-the-support-costs-for-participating-in-the-condo-program-"></span>
    <p>A. The monthly cluster support, colocation and network fees are waived for researchers who buy into the Condo. Essentially, the institution waives those costs in exchange for excess compute cycles. Each user of the system receives a 10 GB storage allocation, which includes backups. Condo groups are also eligible to receive additional group storage of 200 GB. In addition, use of the large, shared parallel scratch filesystem is provided at no cost. Condo users needing more storage for persistent data can purchase additional allocations at current <a href="https://technology.berkeley.edu/storage" target="_blank">rates</a>. In addition, users needing <em>very large</em> amounts of persistent storage can also take advantage of the <a href="../../../../services/high-performance-computing/condos/condo-cluster-service">Condo Storage Service</a>.</p>


??? question "Q. How do I purchase compute nodes for the Condo program?"
    <span id="q-how-do-i-purchase-compute-nodes-for-the-condo-program-"></span>
    <p>A. Prospective condo owners are invited to <a href="http://research-it.berkeley.edu/contact" target="_blank">contact us</a>. Our team will work with you to understand your application and to determine if the Condo cluster would be a suitable platform. We will provide an estimate of the costs of the compute nodes and associated InfiniBand network equipment and then work with your Procurement buyer to specify the correct items to order. Participants are expected to contribute the compute nodes and InfiniBand cable.</p>


??? question "Q. How do I get access to my nodes?"
    <span id="q-how-do-i-get-access-to-my-nodes-"></span>
    <p>A. We will set up a floating reservation equivalent to the number of nodes that you contribute to the Condo to provide priority access to you and your users. You can determine the run time limits for your reservation. If you are not using your reservation, then other users will be allowed to run jobs on unused nodes. If you submit a job to run when all nodes are busy, your job will be given priority over all other waiting jobs to run, but your job will have to wait until nodes become free in order to run. We do not do pre-emptive scheduling where running jobs are killed in order to give immediate access to priority jobs.</p>


??? question "Q. I need dedicated or immediate access to my nodes. Can you accommodate that?"
    <span id="q-i-need-dedicated-or-immediate-access-to-my-nodes-can-you-accommodate-that-"></span>
    <p>A. The basic premise of Condo participation is to facilitate the sharing of unused resources. Dedicating or reserving compute resources works counter to sharing, so this is not possible in the Condo model. As an alternative, PIs can purchase nodes and set them up as a Private Pool in the Condo environment, which will allow a researcher to tailor the access and job queues to meet their specific needs. Private Pool compute nodes will share the HPC infrastructure along with the Condo cluster; however, researchers will have to cover the support costs for BRC staff to manage their compute nodes. Rates for Private Pool compute nodes will be determined at a later date.</p>


??? question "Q. How do I "burst" onto more nodes?"
    <span id="q-how-do-i-burst-onto-more-nodes-"></span>
    <p>A. There are two ways to do this. First,&nbsp;Condo users can access more nodes via Savio's preemptable, <a class="toc-filter-processed" href="../../../../services/high-performance-computing/user-guide/running-your-jobs#low-priorityriority">low-priority quality of service option</a>. Second, faculty can obtain a <a href="../../../../services/high-performance-computing/getting-account/faculty-computing-allowance">Faculty Computing Allowance</a>, and their&nbsp;users can then submit jobs to the General queues to run on the compute nodes provided by the institution. (Use of these nodes is subject to the current job queue policies for general institutional access.)</p>

