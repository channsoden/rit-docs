---
title: Storing Data
keywords: high performance computing, berkeley research computing
tags: [hpc]
---

# Storing Data

!!! summary
    By default, each user on Savio is entitled to a 10 GB home directory which receives regular backups; in addition, each Faculty Computing Allowance-using research group receives 30 GB of project space and each Condo-using research group receives 200 GB of project space to hold research specific application software shared among the group's users. All users also have access to the large Savio high performance <a href="http://research-it.berkeley.edu/blog/15/07/10/savio-cluster-storage-quadrupled-support-big-data-research" target="_blank">scratch filesystem</a> for working with non-persistent data.

| Name | Location | Quota | Backup | Allocation | Description | 
| ---- | -------- | ----- | ------ | ---------- | ----------- |
| HOME | /global/home/users/ | 10 GB | Yes | Per User | HOME directory for permanent data |
| GROUP | /global/home/groups/ | 30/200 GB | No | Per Group | GROUP directory for shared data (30 GB for FCA, 200 GB for Condo) |
| SCRATCH | /global/scratch/ | none | No | Per User | SCRATCH directory with Lustre FS. Forthcoming purge policy to delete any file not accessed in 6 months |

<p>For information on making your files accessible to other users (in particular members of your group), see <a href="../../../../services/high-performance-computing/transferring-data/making-files-accessible-group-members-and">these instructions</a>.</p>

Large input/output files and other files used intensively while
running jobs should be placed in your scratch directory (rather than
your home directory or a project directory) to avoid slowing down
access to home directories for other users.

### Scratch storage

<p>Every Savio user has a scratch space located at <code>/global/scratch/&lt;username&gt;</code>. There are no limits on the use of the scratch space, though it is an 1.5 PB resource shared among all users.</p>
<p>Files stored in this space are not backed up; the recommended use cases for scratch storage are:</p>
<ul><li>Working space for your running jobs (temporary files, checkpoint data, etc.)</li>
<li>High performance input and output</li>
<li>Large input/output files</li>
</ul>

!!! danger "Important"
    <strong>Please remember that global scratch storage is a shared resource.</strong> We strongly urge users to regularly clean up their data in global scratch to decrease scratch storage usage. Users with many terabytes of data may be requested to reduce their usage, if global scratch becomes full.

    BRC has an ongoing inactive data purge policy for the scratch area. Any file that has not been accessed in at least 6 months will be subject to deletion. If you need to retain access to data on the cluster for more than 6 months between uses, you can consider purchasing storage space through the <a href="../../../../services/high-performance-computing/condos/condo-storage-service">condo storage program</a>.

<p>Group scratch directories are not provided. Users who would like to
share materials in their scratch directory with other users can <a
href="{{ site.baseurl
}}/services/high-performance-computing/user-guide/transferring-data/making-files-accessible">set
UNIX permissions</a> to allow access to their directories/files.</p>

### Savio condo storage

Berkeley Research Computing (BRC) offers a <a href="../../../../services/high-performance-computing/condos/condo-storage-service">Condo Storage service</a> for researchers who are <a href="../../../../services/high-performance-computing/condos/condo-cluster-service">Savio Condo Cluster</a> contributors and need additional persistent storage to hold their data sets while using the Savio cluster. 

### More storage options

<p>If you need additional storage during the active phase of your research, such as longer-term storage to augment Savio's temporary Scratch storage, or off-premises storage for backing up data, the&nbsp;<a href="http://researchdata.berkeley.edu/active-research-data-storage-guidance-grid" target="_blank">Active Research Data Storage Guidance Grid</a> can help you identify suitable options.</p>

### Assistance with research data management

<p>The campus's <a href="http://researchdata.berkeley.edu/about" target="_blank">Research Data Management (RDM) service</a> offers consulting on managing your research data, which includes the design or improvement of data transfer workflows, selection of storage solutions, and a great deal more. This service is available at no cost to campus researchers. To get started with a consult, please <a href="http://researchdata.berkeley.edu/" target="_blank">contact</a> RDM Consulting.</p>
<p>In addition, you can find both high level guidance on research data management topics and deeper dives into specific subjects on <a href="http://researchdata.berkeley.edu/" target="_blank">RDM's website</a>. (Visit the links in the left-hand sidebar to further explore each of the site's main topics.)</p>

### CGRL storage

<p>The following storage systems are available to CGRL users. For running jobs, compute nodes within a cluster can only directly access the storage as listed below. The DTN can be used to transfer data between the locations accessible to only one cluster or the other, as detailed in the previous section.</p>

| Name | Cluster | Location | Quota | Backup | Allocation | Description |
| ---- | ------- | -------- | ----- | ------ | ---------- | ----------- |
| Home | Both | <code>/global/home/users/$USER</code> | 10 GB | <strong>Yes</strong> | Per User | Home directory ($HOME) for permanent data |
| Scratch | Vector | <code>/clusterfs/vector/scratch/$USER</code> | none | No | Per User | Short-term, large-scale storage for computing |
| Group | Vector | <code>/clusterfs/vector/instrumentData/</code> | 300 GB | No | Per Group | Group-shared storage for computing |
| Scratch | Rosalind (Savio) | <code>/global/scratch/$USER</code> | none | No | Per User | Short-term, large-scale Lustre storage for very high-performance computing |
| Condo User | Rosalind (Savio) | <code>/clusterfs/rosalind/users/$USER</code> | none | No | Per User | Long-term, large-scale user storage |
| Condo Group | Rosalind (Savio) | <code>/clusterfs/rosalind/groups/</code> | none | No | Per Group | Long-term, large-scale group-shared storage |
