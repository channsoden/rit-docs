---
title: Running Your Jobs
keywords: high performance computing, running jobs
tags: [hpc]
---

Here we give details on job submission for various kinds of jobs in both batch (i.e., non-interactive or background) mode and <a href="#Interactive-jobs">interactive mode</a>.

In addition to the key options of account, partition, and time limit (see below), your job script files can also contain <a href="#Job-submission-with-specific-resource-requirements">options to request various numbers of cores, nodes, and/or computational tasks</a>. And there are a variety of <a href="#Additional-job-submission-options">additional options</a> you can specify in your batch files, if desired, such as email notification options when a job has completed. These are all described further below.

<h3 id="Key-options">Key options to set when submitting your jobs</h3>

When submitting a job, the three key options required are the <strong>account</strong>&nbsp;you are submitting under, the <strong>partition</strong>&nbsp;you are submitting to, and a maximum <strong>time</strong> limit for your job. Under some circumstances, a <strong>Quality of Service&nbsp;(QoS)</strong> is also needed.

<ul>
	<li><strong>Account</strong>: (Required) Each job must be submitted as part of an account, which determines which resources you have access to and how your use will be charged. Note that this account name, which you will use in your SLURM job script files, is different from your Linux account name on the cluster. For instance, for a hypothetical example user <code>lee</code>&nbsp;who has access to&nbsp;both&nbsp;the <code>physics</code>&nbsp;condo and to a Faculty Computing Allowance, their available accounts for running jobs on the cluster might be named <code>co_physics</code> and&nbsp;<code>fc_lee</code>, respectively. (See below for a command that you can run to find out what account name(s) you can use in your own job script files.) Users with access to only a single account (often an FCA) do not need to specify an account.</li>
	<li><strong>Partition</strong>: (Required) Each job must be submitted to a particular partition, which is a collection of similar or identical machines that your job will run on. The different partitions on Savio inclode older or newer generations of standard compute nodes, "big memory" nodes, nodes with Graphics Processing Units (GPUs), etc. (See below for a command that you can run to find out what partitions you can use in your own job script files.) For most users, the combination of the account and partition the user chooses will determine the constraints set on their job, such as job size limit, time limit, etc.  Jobs submitted within a partition will be allocated to that partition's set of compute nodes based on the scheduling policy, until all resources within that partition are exhausted. Currently, on the Savio cluster, </li>
	<li><strong>Time</strong>: (Required) A maximum time limit for the job&nbsp;is required under all conditions. When running your job under a QoS that does not have a time limit (such as jobs submitted by the users of most of the cluster's Condos under their priority access QoS), you can specify a sufficiently long time limit value, but this parameter should not be omitted. Jobs submitted without providing a time limit will be rejected by the scheduler.</li>
	<li><strong>QoS</strong>: (Optional) A QoS is a classification that determines what kind of resources your job can use. For most users, your use of a given account and partition implies a particular QoS, and therefore most users do not need to specify a QoS for standard computational jobs. However there are circumstances where a user would specify the QoS.  For instance, there is a QoS option that you can select for running test jobs when you're debugging your code, which further constrains the resources available to your job and thus may reduce its cost. As well, Condo users can select a "lowprio" QoS which can make use of unused resources on the cluster, in exchange for these jobs being subject to termination when needed, in order to free resources for higher priority jobs. (See below for a command that you can run to find out what QoS options you can use in your own job script files.)</li>
</ul>


<h3 id="Batch-jobs">Batch jobs</h3>

When you want to run one of your jobs in batch (i.e. non-interactive or background) mode, you'll enter an <code>sbatch</code>&nbsp;command. As part of that command, you will also specify the name of, or filesystem path to, a SLURM job script file; e.g., <code>sbatch myjob.sh</code>

A job script specifies where and how you want to run your job on the cluster, and ends with the actual command(s) needed to run your job. The job script file looks much like a standard shell script (<code>.sh</code>) file, but also includes one or more lines that specify options for the SLURM scheduler; e.g.

<code>#SBATCH --some-option-here</code>

Although these lines start with hash signs (<code>#</code>), and thus are regarded as comments by the shell, they are nonetheless read and interpreted by the SLURM scheduler.

Here is a minimal example of a job script file that includes the required account, partition, and time options, as well as a qos specification. It will run unattended for up to 30 seconds on one of the compute nodes in the <code>partition_name</code> partition, and will simply print out the words, "hello world":

```
#!/bin/bash
# Job name:
#SBATCH --job-name=test
#
# Account:
#SBATCH --account=account_name
#
# Partition:
#SBATCH --partition=partition_name
#
# Quality of Service:
#SBATCH --qos=qos_name
#
# Wall clock limit:
#SBATCH --time=00:00:30
#
## Command(s) to run:
echo "hello world"
```

In this and other examples, <code>account_name</code>, <code>partition_name</code>, and <code>qos_name</code> are placeholders for actual values you will need to provide. See <a href="#Key-options">Key options to set</a>, above, for information on what values to use in your own job script files.

See <a href="#Job-submission-with-specific-resource-requirements"> Job submission with specific resource requirements</a>, below, for a set of example job script files, each illustrating how to run a specific type of job.

See <a href="#Finding-output">Finding output</a> to learn where output from running your batch jobs can be found. If errors occur when running your batch job, this is the first place to look for these.

<h3 id="Interactive-jobs">Interactive jobs</h3>

In some instances, you may need to use software that requires user interaction, rather than running programs or scripts in batch mode. To do so, you must first start an instance of an interactive shell on a Savio compute node, within which you can then run your software on that node. To run such an interactive job&nbsp;on a compute node, you'll use&nbsp;<code>srun</code>. Here is a&nbsp;basic example&nbsp;that&nbsp;launches an interactive 'bash' shell on that node, and&nbsp;includes the required account and partition options:

<code>[user@ln001 ~]$ srun --pty -A account_name -p partition_name -t 00:00:30 bash -i</code>

Once your job starts, the Linux prompt will change and indicate you are on a compute node rather than a login node:

```
srun: job 669120 queued and waiting for resources
srun: job 669120 has been allocated resources
[user@n0047 ~]
```

<h3 id="cgrl-jobs">CGRL jobs</h3>

<ul><li>The settings for a job in Vector (Note: you don't need to set the "account"): <code>--partition=vector --qos=vector_batch</code></li>
<li>The settings for a job in Rosalind (Savio1): <code>--partition=savio </code>--account=co_rosalind<code> --qos=</code>rosalind_savio_normal</li>
<li>The settings for a job in Rosalind (Savio2 HTC): <code>--partition=savio2_htc </code>--account=co_rosalind<code> --qos=</code>rosalind_htc2_normal</li></ul>

<h3 id="Job-submission-with-specific-resource-requirements">Job submission with specific resource requirements</h3>

This section details options for specifying the resource requirements for you jobs. We also provide a variety of [example job scripts](../../../../../services/high-performance-computing/user-guide/running-your-jobs/scheduler-examples) for setting up parallelization, low-priority jobs, jobs using fewer cores than available on a node, and long-running FCA jobs.


Remember that nodes are assigned for exclusive access by your job, except in the "savio2_htc" and "savio2_gpu" partitions. So, if possible, you generally want to set SLURM options and write your code to use all the available resources on the nodes assigned to your job (e.g., 20 cores and 64 GB memory per node in the "savio" partition).

<h4 id="Memory">Memory available</h4>

Also note that in all partitions except for GPU and&nbsp;HTC partitions, by default the full memory on the node(s) will be available to your job. On&nbsp;the GPU and HTC partitions you get an amount of memory proportional to the number of cores your job requests relative to the number of cores on the node. For example, if the node has 64 GB and 8 cores, and you request 2 cores, you'll have access to 16 GB memory. If you need more memory than that, you should request additional cores. Please do not request memory using the memory-related flags available for sbatch and srun.

<h4 id="Parallelization">Parallelization</h4>

When submitting parallel code, you usually need to specify the number of tasks, nodes, and CPUs to be used by your job in various ways. For any given use case, there are generally multiple ways to set the options to achieve the same effect; these examples try to illustrate what we consider to be best practices.

The key options for parallelization are:

<ul>
	<li><code>--nodes</code>&nbsp;(or <code>-N</code>): indicates the number of nodes to use</li>
	<li><code>--ntasks-per-node</code>: indicates the number of tasks (i.e., processes one wants to run on each node)</li>
	<li><code>--cpus-per-task</code>&nbsp;(or <code>-c</code>): indicates the number of cpus to be used for each task</li>
</ul>

In addition, in some cases it can make sense to use the <code>--ntasks</code>&nbsp;(or <code>-n</code>) option to indicate the total number of tasks and let the scheduler determine how many nodes and tasks per node are needed. In general --cpus-per-task&nbsp;will be 1 except when running threaded code. &nbsp;

Note that if the various options are not set, SLURM will in some cases infer what the value of the option needs to be given other options that are set and in other cases will treat the value as being 1. So some of the options set in the example below are not strictly necessary, but we give them explicitly to be clear.

Here's an example script that requests an entire Savio node and specifies 20 cores per task. 

```
#!/bin/bash
#SBATCH --job-name=test
#SBATCH --account=account_name
#SBATCH --partition=savio
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=20
#SBATCH --time=00:00:30
## Command(s) to run:
echo "hello world"
```

Only the partition, time, and account flags are required. And, strictly speaking, the account will default to a default account if you don't specify it, so in some cases that can be omitted.


<h4 id="low-priority">Low priority jobs via a condo account</h4>

<p>As a condo contributor, are entitled to use the extra resource that is available on the SAVIO cluster (across all partitions). This is done through a low priority QoS "savio_lowprio" and your account is automatically subscribed to this QoS during the account creation stage. You do not need to request it explicitly. By using this QoS you are no longer limited by your condo size. What this means to users is that you will now have access to the broader compute resource which is limited by the size of partitions. The per-job limit is 24 nodes and 3 days runtime. Additionally, these jobs can be run on all types and generations of hardware resources in the Savio cluster, not just those matching the condo contribution. At this time, there is no limit or allowance applied to this QoS, so condo contributors are free to use as much of the resource as they need.</p>
<p>However this QoS does not get a priority as high as the general QoSs, such as "savio_normal" and "savio_debug", or all the condo QoSs, and it is subject to preemption when all the other QoSs become busy. Thus it has two implications:</p>
<ol><li>When system is busy, any job that is submitted with this QoS will be pending and yield to other jobs with higher priorities.</li>
<li>When system is busy and there are higher priority jobs pending, the scheduler will preempt jobs that are running with this lower priority QoS. At submission time, the user can choose whether a preempted jobs should simply be killed or should be automatically requeued after it's killed. Please note that, since preemption could happen at any time, it is very beneficial if your job is capable of <a href="https://en.wikipedia.org/wiki/Application_checkpointing#Checkpoint/Restart">checkpointing/restarting</a> by itself, when you choose to requeue the job. Otherwise, you may need to verify data integrity manually before you want to run the job again.</li>
</ol>

We provide an [example job script for such jobs](../../../../../services/high-performance-computing/user-guide/running-your-jobs/scheduler-examples#example-lowprio)

<h4 id="long-running">Long-running jobs via an FCA</h4>

Most jobs running under an FCA have a maximum time limit of 72 hours (three days). However users can run jobs using a small number of cores in the long queue, using the `savio2_htc` partition and the  `savio_long` QoS.

A given job in the long queue can use no more than 4 cores and a maximum of 10 days. Collectively across the entire Savio cluster, at most 24 cores are available for long-running jobs, so you may find that your job may sit in the queue for a while before it starts.

We provide an [example job script for such jobs](../../../../../services/high-performance-computing/user-guide/running-your-jobs/scheduler-examples#example-long)


<h3 id="Additional-job-submission-options">Additional job submission options</h3>

Here are some additional options that you can incorporate as needed into your own scripts. For the full set of available options, please see the <a href="http://slurm.schedmd.com/sbatch.html" target="_blank"> SLURM documentation on the sbatch command</a>.

<h4 id="Finding-output">Output options</h4>

Output from running a SLURM batch job is, by default, placed in a log file named <code>slurm-%j.out</code>, where the job's ID is substituted for <code>%j</code>; e.g. <code>slurm-478012.out</code> This file will be created in your current directory; i.e. the directory from within which you entered the sbatch command. Also by default, both command output and error output (to stdout and stderr, respectively) are combined in this file.

To specify alternate files for command and error output use:

<ul>
	<li><code>--output</code>: destination file for stdout</li>
	<li><code>--error</code>: destination file for stderr</li>
</ul>

<h4 id="Email-options">Email Notifications</h4>

By specifying your email address, the SLURM scheduler can email you when the status of your job changes. Valid options are BEGIN, END, FAIL, REQUEUE, and ALL, and multiple options can be separated by commas.

The required options for email notifications are:

<ul>
	<li><code>--mail-type</code>: when you want to be notified</li>
	<li><code>--mail-user</code>: your email address</li>
</ul>

<h4 id="Email-example">Submission with output and email options</h4>

<pre>
#!/bin/bash
#SBATCH --job-name=test
#SBATCH --account=account_name
#SBATCH --partition=savio
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=20
#SBATCH --time=00:00:30
<strong>#SBATCH --output=test_job_%j.out</strong>
<strong>#SBATCH --error=test_job_%j.err</strong>
<strong>#SBATCH --mail-type=END,FAIL</strong>
<strong>#SBATCH --mail-user=jessie.doe@berkeley.edu</strong>
## Command(s) to run:
echo "hello world"
</pre>

<h4 id="QoS-options">QoS options</h4>

While your job will use a default QoS, generally <code>savio_normal</code>, you can specify a different QoS, such as the debug QoS for short jobs, using the <code>--qos</code> flag, e.g.,

<pre>
#!/bin/bash
#SBATCH --job-name=test
#SBATCH --account=account_name
#SBATCH --partition=savio
<strong>#SBATCH --qos=savio_debug</strong>
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=20
##SBATCH --time=00:00:30
## Command(s) to run:
echo "hello world"
</pre>

<h3 id="embarrassingly-parallel">Bundling tasks into a single job to use all the cores on a node</h3>

Many users have multiple jobs that each use only a single core or a small number of cores and therefore cannot take advantage of all the cores on a Savio node.
There are two tools that allow one to automate the parallelization of such jobs, in particular
allowing one to group jobs into a single SLURM submission to take advantage of the multiple cores on
a given Savio node.

For this purpose, we recommend the use of the community-supported <a href="../../../../../services/high-performance-computing/user-guide/running-your-jobs/gnu-parallel">GNU parallel tool</a>. One can instead use Savio's <a href="../../../../../services/high-performance-computing/user-guide/running-your-jobs/hthelper-script">HT Helper tool</a>, but for users not already familiar with either tool, we recommend GNU parallel.

<h3 id="Array-jobs">Job arrays</h3>

Job arrays allow many jobs to be submitted simultaneously with the same requirements. Within each job the environment variable <code>$SLURM_ARRAY_TASK_ID</code> is set and can be used to alter the execution of each job in the array. Note that as is true for regular jobs, each job in the array will be allocated one or more entire nodes (except for the HTC or GPU partitions), so job arrays are not a way to bundle multiple tasks to run on a single node.  

By default, output from job arrays is placed in a series of log files named <code>slurm-%A_%a.out</code>, where <code>%A</code> is the overall job ID and <code>%a</code> is the task ID.

For example, the following script would write "I am task 0" to <code>array_job_XXXXXX_task_0.out</code>, "I am task 1" to <code>array_job_XXXXXX_task_1.out</code>, etc, where XXXXXX is the job ID.

<pre>
#!/bin/bash
#SBATCH --job-name=test
#SBATCH --account=account_name
#SBATCH --partition=savio
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=20
#SBATCH --time=00:00:30
<strong>#SBATCH --array=0-31</strong>
<strong>#SBATCH --output=array_job_%A_task_%a.out</strong>
<strong>#SBATCH --error=array_job_%A_task_%a.err</strong>
## Command(s) to run:
echo "I am task $SLURM_ARRAY_TASK_ID"
</pre>

<h3 id="Checkpointing">Checkpointing/Restarting your jobs</h3>

Checkpointing/restarting is the technique where the applications’ state is stored in the filesystem, allowing the user to restart computation from this saved state in order to minimize loss of computation time, for example, when the job reaches its allowed wall clock limit, the job is preempted, or when software/hardware faults occur, etc. By checkpointing or saving intermediate results frequently the user won't lose as much work if their jobs are preempted or otherwise terminated prematurely for some reason.

If you wish to do checkpointing, your first step should always be to check if your application already has such capabilities built-in, as that is the most stable and safe way of doing it. Applications that are known to have some sort of native checkpointing include:
Abaqus, Amber, Gaussian, GROMACS, LAMMPS, NAMD, NWChem, Quantum Espresso, STAR-CCM+, and VASP.

In case your program does not natively support checkpointing, it may also be worthwhile to consider utilizing generic checkpoint/restart solutions if and as needed that should work application-agnostic. One example of such a solution is <a href="https://github.com/dmtcp/dmtcp" target="_blank">DMTCP: Distributed MultiThreaded CheckPointing</a>. DMTCP is a checkpoint-restart solution that works outside the flow of user applications, enabling their state to be saved without application alterations. You can find its <a href="https://github.com/dmtcp/dmtcp/blob/master/QUICK-START.md" target="_blank">reference quick-start documentation here</a>.

<h3 id="Migration">Migrating from other schedulers to SLURM</h3>

We provide <a href="../../../../../services/high-performance-computing/running-your-jobs#Migration">some tips on migrating to SLURM for users familiar with Torque/PBS</a>.

For users coming from other schedulers, such as Platform LSF, SGE/OGE, Load Leveler, please use <a href="http://slurm.schedmd.com/rosetta.pdf" target="_blank">this link</a> to find a quick reference.

