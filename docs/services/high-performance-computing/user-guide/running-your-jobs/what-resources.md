---
title: What Resources Can My Job Use and What Are the Charges?
keywords: high performance computing
tags: 
---

<h3 id="Charges">Charges for running jobs</h3>

When running your SLURM batch or interactive jobs on the Savio cluster under a <a href="../../../../../services/high-performance-computing/getting-account/faculty-computing-allowance">Faculty Computing Allowance</a> account (i.e. a scheduler account whose name begins with <code>fc_</code>), your usage of computational time is tracked (in effect, "charged" for, although no costs are incurred) via abstract measurement units called "Service Units." (Please see <a href="../../../../../services/high-performance-computing/service-units-savio">Service Units on Savio</a> for a description of how this usage is calculated.) When all of the Service Units provided under an Allowance have been <a href="../../../../../services/high-performance-computing/getting-account/options-when-fca-exhausted">exhausted</a>, no more jobs can be run under that account. To check your usage or total usage under an FCA, please use our <a href="../../../../../services/high-performance-computing/frequently-asked-questions#q-how-can-i-check-on-my-faculty-computing-allowance-fca-usage-">check_usage.sh script</a>.

Please also note that, when running jobs on many of Savio's pools of compute nodes, you are provided with exclusive access to those nodes, and thus are "charged" for using all of that node's cores. For example, if you run a job for one hour on a standard 24-core compute node on the savio2 partition, your job will always be charged for using 24 core hours, even if it requires just a single core or a few cores. (For more details, including information on ways you can most efficiently use your computational time on the cluster, please see the <a href="../../../../../services/high-performance-computing/service-units-savio#Scheduling">Scheduling Nodes v. Cores</a> section of <a href="../../../../../services/high-performance-computing/service-units-savio">Service Units on Savio</a>.)

Usage tracking does not affect jobs run under a <a href="../../../../../services/high-performance-computing/condos/condo-cluster-service">Condo</a> account (i.e. a scheduler account whose name begins with <code>co_</code>), which has no Service Unit-based limits.
<h3 id="Key-options">What resources (hardware) your jobs have access to</h3>


You can view the accounts you have access to, partitions you can use, and the QoS&nbsp;options&nbsp;available to you using the <code>sacct</code><code>mgr</code>&nbsp;command:

<code>sacctmgr -p show associations user=$USER</code>

This will return output such as the following for a hypothetical example user <code>lee</code>&nbsp;who has access to&nbsp;both&nbsp;the <code>physics</code>&nbsp;condo and to a Faculty Computing Allowance. Each line of this output indicates a specific combination of an account, a partition, and QoSes that you can use in a job script file, when submitting any individual batch job:

<code>Cluster|Account|User|Partition|...|QOS|Def QOS|GrpTRESRunMins|
brc|co_physics|lee|savio2_1080ti|...|savio_lowprio|savio_lowprio||
brc|co_physics|lee|savio2_knl|...|savio_lowprio|savio_lowprio||
brc|co_physics|lee|savio2_bigmem|...|savio_lowprio|savio_lowprio||
brc|co_physics|lee|savio2_gpu|...|savio_lowprio|savio_lowprio||
brc|co_physics|lee|savio2_htc|...|savio_lowprio|savio_lowprio||
brc|co_physics|lee|savio_bigmem|...|savio_lowprio|savio_lowprio||</code> <code><strong>brc|co_physics|lee|savio2|...|physics_savio2_normal,savio_lowprio|physics_savio2_normal||</strong></code> <code>brc|co_physics|lee|savio|...|savio_lowprio|savio_lowprio||
brc|fc_lee|lee|savio2_1080ti|...|savio_debug,savio_normal|savio_normal||
brc|fc_lee|lee|savio2_knl|...|savio_debug,savio_normal|savio_normal||
brc|fc_lee|lee|savio2_bigmem|...|savio_debug,savio_normal|savio_normal||
brc|fc_lee|lee|savio2_gpu|...|savio_debug,savio_normal|savio_normal||
brc|fc_lee|lee|savio2_htc|...|savio_debug,savio_normal|savio_normal||
brc|fc_lee|lee|savio_bigmem|...|savio_debug,savio_normal|savio_normal||
brc|fc_lee|lee|savio2|...|savio_debug,savio_normal|savio_normal||
brc|fc_lee|lee|savio|...|savio_debug,savio_normal|savio_normal||</code>

The <code>Account</code>, <code>Partition</code>, and&nbsp;<code>QOS</code> fields indicate which partitions and QoSes you have access to under each of your account(s). The <code>Def QoS</code>&nbsp;field identifies the default QoS that will be used if you do not explicitly identify a QoS when submitting a job. Thus as per the example above, if the user <code>lee</code> submitted a batch job using their <code>fc_lee</code> account, they could submit their job to either the <code>savio2_gpu</code>, <code> savio2_htc</code>, <code>savio2_bigmem</code>, <code>savio2</code>, <code>savio</code>, or <code>savio_bigmem</code> partitions. (And when doing so, they could also choose either the <code>savio_debug</code> or <code>savio_normal</code> QoS, with a default of <code>savio_normal</code> if no QoS was specified.)

If you are running your job in a condo, be sure to note which of the line(s) of output associated with the condo account (those beginning with "co_" ) have their <code>Def QoS</code>&nbsp;being a lowprio QoS and which have a normal QoS. Those with a normal QoS (such as the line highlighted in <strong>boldface text</strong> in the above example) are the QoS to which you have priority access, while those with a lowprio QoS are those to which you have only low priority access. Thus, in the above example, the user <code>lee</code> should select the <code>co_physics</code> account and the&nbsp;<code>savio2</code> partition when they want to run jobs with normal priority, using the resources available via their condo membership.

<p>You can find more details on the hardware specifications for the machines in the  various partitions here for the <a href="../../../../../services/high-performance-computing/user-guide/hardware-config#Savio-Hardware">Savio</a> and <a href="../../../../../services/high-performance-computing/user-guide/hardware-config#CGRL-hardware">CGRL (Vector/Rosalind)</a> clusters.</p>

<p>You can find more details on each partition and the QoS available in those partitions here for the <a href="../../../../../services/high-performance-computing/user-guide/running-your-jobs/scheduler-config#Savio-Scheduler-Configuration">Savio</a> and <a href="../../../../../services/high-performance-computing/user-guide/running-your-jobs/scheduler-config#CGRL-scheduler">CGRL (Vector/Rosalind)</a> clusters.</p>

