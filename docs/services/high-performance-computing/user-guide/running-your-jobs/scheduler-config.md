---
title: Scheduler configuration
keywords: high performance computing
tags: [hpc, cgrl, gpu, dl, deeplearning, ai]
---

<h3 id="Savio-Scheduler-Configuration">Savio scheduler configuration</h3>

<h4 id="Savio-Partitions">Savio partitions</h4>

| Partition      | Nodes | Node Features | Nodes shared? | SU/core hour  ratio |
| ------------- | ------: | ---------------- | --------- | ------ |
|savio|160|savio|exclusive|0.75|
|savio_bigmem|4|savio_bigmem or savio_m512|exclusive|1.67|
|savio2|163|savio2 or savio2_c24 or savio2_c28|exclusive|1.00|
|savio2_bigmem|36|savio2_bigmem or savio2_m128|exclusive|1.20|
|savio2_htc|	20	|savio2_htc|shared|	1.20|
|savio2_gpu|	17|	savio2_gpu|	shared|	2.67 (5.12 / GPU)|
|savio2_1080ti|	7|	savio2_1080ti|	shared|	1.67 (3.34 / GPU)|
|savio2_knl|	28|	savio2_knl|	exclusive|	0.40|
|savio3|	116	|savio3	|exclusive	|TBD|
|savio3_bigmem	|16|	savio3_bigmem|	exclusive	|TBD|
|savio3_xlmem|	2|	savio3_xlmem|	exclusive|	TBD|
|savio3_gpu|	1|	savio3_gpu|	shared|	TBD|
|savio3_2080ti|	8|	4rtx,8rtx|	shared|	TBD|

<h4 id="Savio-general-QoS">Overview of QoS configurations for Savio</h4>

For details on specific Condo QoS configurations, <a href="{{
site.baseurl }}/services/high-performance-computing/user-guide/running-your-jobs/scheduler-config#Savio-condo-QoS">see below</a>.

| QoS | Accounts allowed | QoS Limits | Partitions |
| ---------------------- |-----| ----------------|----|
| savio_normal |FCA*, ICA|24 nodes max per job,72:00:00 wallclock limit|all**|
| savio_debug |FCA*, ICA|4 nodes max per job, 4 nodes in total,00:30:00 wallclock limit|all**|
| savio_long |FCA*, ICA|4 cores max per job, 24 cores in total,10 day wallclock limit|savio2_htc|
| Condo QoS |condos|specific to each condo, see next section|as purchased|
| savio_lowprio |condos|24 nodes max per job,72:00:00 wallclock limit|all|

(*) Including purchases of additional SUs for an FCA.

(**) Note that savio3 nodes (including the various bigmem, GPU,
etc. nodes) are not yet available for use by FCAs or ICAs.

<h4 id="Savio-condo-QoS">QoS configurations for Savio condos</h4>

<h5><a name="Savio_Condo" id="Savio_Condo">Savio Condo QoS Configurations</a></h5>
<table align="center" border="1" cellspacing="0"><tbody><tr><th style="text-align:left">Account</th>
<th style="text-align:left">QoS</th>
<th style="text-align:left">QoS Limit</th>
</tr><tr><td style="text-align:left; vertical-align:middle">co_acrb</td>
<td style="text-align:left; vertical-align:middle">acrb_savio_normal</td>
<td style="text-align:left; vertical-align:middle">8 nodes max per group</td>
</tr><tr><td style="text-align:left; vertical-align:middle">co_aiolos</td>
<td style="text-align:left; vertical-align:middle">aiolos_savio_normal</td>
<td style="text-align:left; vertical-align:middle">12 nodes max per group<br>
24:00:00 wallclock limit</td>
</tr><tr><td style="text-align:left; vertical-align:middle">co_astro</td>
<td style="text-align:left; vertical-align:middle">
    <p>astro_savio_debug</p>
    <hr>
    <p>astro_savio_normal</p>
</td>
<td style="text-align:left; vertical-align:middle">
    <p>4 nodes max per group<br>4 nodes max per job<br>00:30:00 wallclock limit</p>
    <hr>
    <p>32 nodes max per group<br>16 nodes max per job</p> 
</td>
</tr><tr><td style="text-align:left; vertical-align:middle">co_dlab</td>
<td style="text-align:left; vertical-align:middle">dlab_savio_normal</td>
<td style="text-align:left; vertical-align:middle">4 nodes max per group</td>
</tr><tr><td style="text-align:left; vertical-align:middle">co_nuclear</td>
<td style="text-align:left; vertical-align:middle">nuclear_savio_normal</td>
<td style="text-align:left; vertical-align:middle">24 nodes max per group</td>
</tr><tr><td style="text-align:left; vertical-align:middle">co_praxis</td>
<td style="text-align:left; vertical-align:middle">praxis_savio_normal</td>
<td style="text-align:left; vertical-align:middle">4 nodes max per group</td>
</tr><tr><td style="text-align:left; vertical-align:middle">co_rosalind</td>
<td style="text-align:left; vertical-align:middle">rosalind_savio_normal</td>
<td style="text-align:left; vertical-align:middle">8 nodes max per group<br>
4 nodes max per job per user</td>
</tr></tbody></table><h5><a name="Savio_Bigmem_Condo" id="Savio_Bigmem_Condo">Savio Bigmem Condo QoS Configurations</a></h5>
<p>No condos in this partition.</p>
<h5><a name="Savio2_Condo" id="Savio2_Condo">Savio2 Condo QoS Configurations</a></h5>
<table align="center" border="1" cellspacing="0"><tbody><tr><th style="text-align:left">Account</th>
<th style="text-align:left">QoS</th>
<th style="text-align:left">QoS Limit</th>
</tr><tr><td style="text-align:left; vertical-align:middle">co_biostat</td>
<td style="text-align:left; vertical-align:middle">biostat_savio2_normal</td>
<td style="text-align:left; vertical-align:middle">20 nodes max per group</td>
</tr><tr><td style="text-align:left; vertical-align:middle">co_chemqmc</td>
<td style="text-align:left; vertical-align:middle">chemqmc_savio2_normal</td>
<td style="text-align:left; vertical-align:middle">16 nodes max per group</td>
</tr><tr><td style="text-align:left; vertical-align:middle">co_dweisz</td>
<td style="text-align:left; vertical-align:middle">dweisz_savio2_normal</td>
<td style="text-align:left; vertical-align:middle">8 nodes max per group</td>
</tr><tr><td style="text-align:left; vertical-align:middle">co_econ</td>
<td style="text-align:left; vertical-align:middle">econ_savio2_normal</td>
<td style="text-align:left; vertical-align:middle">2 nodes max per group</td>
</tr><tr><td style="text-align:left; vertical-align:middle">co_hiawatha</td>
<td style="text-align:left; vertical-align:middle">hiawatha_savio2_normal</td>
<td style="text-align:left; vertical-align:middle">40 nodes max per group</td>
</tr><tr><td style="text-align:left; vertical-align:middle">co_lihep</td>
<td style="text-align:left; vertical-align:middle">lihep_savio2_normal</td>
<td style="text-align:left; vertical-align:middle">4 nodes max per group</td>
</tr><tr><td style="text-align:left; vertical-align:middle">co_mrirlab</td>
<td style="text-align:left; vertical-align:middle">mrirlab_savio2_normal</td>
<td style="text-align:left; vertical-align:middle">4 nodes max per group</td>
</tr><tr><td style="text-align:left; vertical-align:middle">co_planets</td>
<td style="text-align:left; vertical-align:middle">planets_savio2_normal</td>
<td style="text-align:left; vertical-align:middle">4 nodes max per group</td>
</tr><tr><td style="text-align:left; vertical-align:middle">co_stat</td>
<td style="text-align:left; vertical-align:middle">stat_savio2_normal</td>
<td style="text-align:left; vertical-align:middle">2 nodes max per group</td>
</tr><tr><td style="text-align:left; vertical-align:middle">co_bachtrog</td>
<td style="text-align:left; vertical-align:middle">bachtrog_savio2_normal</td>
<td style="text-align:left; vertical-align:middle">4 nodes max per group</td>
</tr><tr><td style="text-align:left; vertical-align:middle">co_noneq</td>
<td style="text-align:left; vertical-align:middle">noneq_savio2_normal</td>
<td style="text-align:left; vertical-align:middle">8 nodes max per group</td>
</tr><tr><td style="text-align:left; vertical-align:middle">co_kranthi</td>
<td style="text-align:left; vertical-align:middle">kranthi_savio2_normal</td>
<td style="text-align:left; vertical-align:middle">4 nodes max per group</td>
</tr></tbody></table><h5><a name="Savio2_Bigmem_Condo" id="Savio2_Bigmem_Condo">Savio2 Bigmem Condo QoS Configurations</a></h5>
<table align="center" border="1" cellspacing="0"><tbody><tr><th style="text-align:left">Account</th>
<th style="text-align:left">QoS</th>
<th style="text-align:left">QoS Limit</th>
</tr><tr><td style="text-align:left; vertical-align:middle">co_laika</td>
<td style="text-align:left; vertical-align:middle">laika_bigmem2_normal</td>
<td style="text-align:left; vertical-align:middle">4 nodes max per group</td>
</tr><tr><td style="text-align:left; vertical-align:middle">co_dweisz</td>
<td style="text-align:left; vertical-align:middle">dweisz_bigmem2_normal</td>
<td style="text-align:left; vertical-align:middle">4 nodes max per group</td>
</tr><tr><td style="text-align:left; vertical-align:middle">co_aiolos</td>
<td style="text-align:left; vertical-align:middle">aiolos_bigmem2_normal</td>
<td style="text-align:left; vertical-align:middle">4 nodes max per group<br>
24:00:00 wallclock limit</td>
</tr><tr><td style="text-align:left; vertical-align:middle">co_bachtrog</td>
<td style="text-align:left; vertical-align:middle">bachtrog_bigmem2_normal</td>
<td style="text-align:left; vertical-align:middle">4 nodes max per group</td>
</tr><tr><td style="text-align:left; vertical-align:middle">co_msedcc</td>
<td style="text-align:left; vertical-align:middle">msedcc_bigmem2_normal</td>
<td style="text-align:left; vertical-align:middle">8 nodes max per group</td>
</tr></tbody></table><h5><a name="Savio2_HTC_Condo" id="Savio2_HTC_Condo">Savio2 HTC Condo QoS Configurations</a></h5>
<table align="center" border="1" cellspacing="0"><tbody><tr><th style="text-align:left">Account</th>
<th style="text-align:left">QoS</th>
<th style="text-align:left">QoS Limit</th>
</tr><tr><td style="vertical-align: middle;">co_rosalind</td>
<td style="vertical-align: middle;">rosalind_htc2_normal</td>
<td style="vertical-align: middle;">8 nodes max per group</td>
</tr></tbody></table><h5><a name="Savio2_GPU_Condo" id="Savio2_GPU_Condo">Savio2 GPU Condo QoS Configurations</a></h5>
<table align="center" border="1" cellspacing="0"><tbody><tr><th style="text-align:left">Account</th>
<th style="text-align:left">QoS</th>
<th style="text-align:left">QoS Limit</th>
</tr><tr><td style="text-align:left; vertical-align:middle">co_acrb</td>
<td style="text-align:left; vertical-align:middle">acrb_gpu2_normal</td>
<td style="text-align:left; vertical-align:middle">44 GPUs max per group</td>
</tr><tr><td style="text-align:left; vertical-align:middle">co_stat</td>
<td style="text-align:left; vertical-align:middle">stat_gpu2_normal</td>
<td style="text-align:left; vertical-align:middle">8 GPUs max per group</td>
</tr></tbody></table><h5><a name="Savio2_1080ti_Condo" id="Savio2_1080ti_Condo">Savio2 1080ti Condo QoS Configurations</a></h5>
<table align="center" border="1" cellspacing="0"><tbody><tr><th style="text-align:left">Account</th>
<th style="text-align:left">QoS</th>
<th style="text-align:left">QoS Limit</th>
</tr><tr><td style="vertical-align: middle;">co_acrb</td>
<td style="vertical-align: middle;">acrb_1080ti2_normal</td>
<td style="vertical-align: middle;">12 GPUs max per group</td>
</tr><tr><td style="vertical-align: middle;">co_mlab</td>
<td style="vertical-align: middle;">mlab_1080ti2_normal</td>
<td style="vertical-align: middle;">16 GPUs max per group</td>
</tr></tbody></table><h5><a name="Savio2_KNL_Condo" id="Savio2_KNL_Condo">Savio2 KNL Condo QoS Configurations</a></h5>
<table align="center" border="1" cellspacing="0"><tbody><tr><th style="text-align:left">Account</th>
<th style="text-align:left">QoS</th>
<th style="text-align:left">QoS Limit</th>
</tr><tr><td style="vertical-align: middle;">co_lsdi</td>
<td style="vertical-align: middle;">lsdi_knl2_normal</td>
<td style="vertical-align: middle;">28 nodes max per group<br>
5 running jobs max per user<br>
20 total jobs max per user</td>
</tr></tbody></table><h5><a name="Savio3_Condo" id="Savio3_Condo">Savio3 Condo QoS Configurations</a></h5>
<table align="center" border="1" cellspacing="0"><tbody><tr><th style="text-align:left">Account</th>
<th style="text-align:left">QoS</th>
<th style="text-align:left">QoS Limit</th>
</tr><tr><td style="vertical-align: middle;">co_chemqmc</td>
<td style="vertical-align: middle;">chemqmc_savio3_normal</td>
<td style="vertical-align: middle;">4 nodes max per group</td>
</tr><tr><td style="vertical-align: middle;">co_laika</td>
<td style="vertical-align: middle;">laika_savio3_normal</td>
<td style="vertical-align: middle;">4 nodes max per group</td>
</tr><tr><td style="vertical-align: middle;">co_noneq</td>
<td style="vertical-align: middle;">noneq_savio3_normal</td>
<td style="vertical-align: middle;">8 nodes max per group</td>
</tr><tr><td style="vertical-align: middle;">co_aiolos</td>
<td style="vertical-align: middle;">aiolos_savio3_normal</td>
<td style="vertical-align: middle;">36 nodes max per group<br>
24:00:00 wallclock limit</td>
</tr></tbody></table><h5><a name="Savio3_Bigmem_Condo" id="Savio3_Bigmem_Condo">Savio3 Bigmem Condo QoS Configurations</a></h5>
<table align="center" border="1" cellspacing="0"><tbody><tr><th style="text-align:left">Account</th>
<th style="text-align:left">QoS</th>
<th style="text-align:left">QoS Limit</th>
</tr><tr><td style="vertical-align: middle;">co_genomicdata</td>
<td style="vertical-align: middle;">genomicdata_bigmem3_normal</td>
<td style="vertical-align: middle;">4 nodes max per group</td>
</tr></tbody></table><h5><a name="Savio3_Xlmem_Condo" id="Savio3_Xlmem_Condo">Savio3 Xlmem Condo QoS Configurations</a></h5>
<table align="center" border="1" cellspacing="0"><tbody><tr><th style="text-align:left">Account</th>
<th style="text-align:left">QoS</th>
<th style="text-align:left">QoS Limit</th>
</tr><tr><td style="vertical-align: middle;">co_genomicdata</td>
<td style="vertical-align: middle;">genomicdata_xlmem3_normal</td>
<td style="vertical-align: middle;">1 nodes max per group</td>
</tr></tbody></table>


<h3 id="CGRL-scheduler">CGRL scheduler configuration</h3>

<p>The clusters uses the SLURM scheduler to manage jobs. When submitting your jobs via <code>sbatch</code> or <code>srun</code> commands, use the following SLURM options:</p>
<ul><li>The settings for a job in Vector (Note: you don't need to set the "account"): <code>--partition=vector --qos=vector_batch</code></li>
<li>The settings for a job in Rosalind (Savio1): <code>--partition=savio --account=co_rosalind --qos=rosalind_savio_normal</code></li>
<li>The settings for a job in Rosalind (Savio2 HTC): <code>--partition=savio2_htc --account=co_rosalind --qos=rosalind_htc2_normal</code></li>
<p><strong>NOTE:</strong> To check which QoS you are allowed to use, simply run "sacctmgr -p show associations user=$USER"</p>
Here are the details for each CGRL partition and associated QoS.
<table id="scheduler-config-cgrl-table" border="1" align="center"><tbody><tr style="background-color:#D3D3D3"><th>Partition</th>
<th>Account</th>
<th>Nodes</th>
<th>Node List</th>
<th>Node Feature</th>
<th>QoS</th>
<th>QoS Limit</th>
</tr><tr><td rowspan="4">vector</td>
<td rowspan="4">&nbsp;</td>
<td rowspan="4">11</td>
<td>n00[00-03].vector0</td>
<td>vector,vector_c12,vector_m96</td>
<td rowspan="4">vector_batch</td>
<td rowspan="4">48 cores max per job&nbsp;&nbsp;&nbsp;&nbsp;
<p>96 cores max per user</p></td>
</tr><tr><td style="padding-left: 20px">n0004.vector0</td>
<td>vector,vector_c48,vector_m256</td>
</tr><tr><td style="padding-left: 20px">n00[05-08].vector0</td>
<td>vector,vector_c16,vector_m128</td>
</tr><tr><td style="padding-left: 20px">n00[09]-n00[10].vector0</td>
<td>vector,vector_c12,vector_m48</td>
</tr><tr><td>savio</td>
<td>co_rosalind</td>
<td>8</td>
<td>n0[000-095].savio1, n0[100-167].savio1</td>
<td>savio</td>
<td>rosalind_savio_normal</td>
<td>8 nodes max per group</td>
</tr><tr><td>savio2_htc</td>
<td>co_rosalind</td>
<td>8</td>
<td>n0[000-011].savio2, n0[215-222].savio2</td>
<td>savio2_htc</td>
<td>rosalind_htc2_normal</td>
<td>8 nodes max per group</td>
</tr></tbody></table>
