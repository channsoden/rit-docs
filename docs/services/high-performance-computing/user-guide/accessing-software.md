---
title: Software Available on Savio
keywords: high performance computing, berkeley research computing
tags: [hpc, software, modules]
---

## Overview

Savio provides a variety of software installed by the system administrators and BRC consultants, including compilers, interpreters, and tools for statistical analysis, data visualization, bioinformatics, computational biology, and other sciences. You can access much of the software available on the Savio cluster using [Environment Module commands](#accessing-software-using-environment-modules). These commands allow you to display currently loaded and [available software packages](#software-provided-on-savio) as well as to  load and unload the packages and their associated runtime environments.

The Environment Module system allows users to manage their runtime environments dynamically on the Savio cluster. This is accomplished by loading and unloading modulefiles which contain the application specific information for setting a user’s environment, primarily the shell environment variables, such as _PATH_, _LD_LIBRARY_PATH_, etc. The module system is useful in managing potentially conflicting applications, as well as different versions of the same application, especially in a shared computing environment.

In addition to the software provided on the Savio cluster you are welcome to [install your own software](installing-software).

## Accessing Software Using Environment Modules

All of the module commands begin with `module` and are followed by a subcommand. (In this list of commands, a vertical bar (“|”) means “or”, e.g., `module add` and `module load` are equivalent. You'll need to substitute actual modulefile names for _`modulefile`_, _`modulefile1`_, and _`modulefile2` _in the examples below.)

*   `module avail` - List all available modulefiles in the current MODULEPATH (i.e. available to you).
*   `module list` - List modules loaded into your environment.
*   `module add|load _modulefile_ ...` - Load modulefile(s) into your environment.
*   `module rm|unload _modulefile_ ...` - Remove modulefile(s) from your environment.
*   `module swap|switch [_modulefile1_] _modulefile2_ -` Switch loaded `_modulefile1_` with `_modulefile2_.`
*   `module show|display _modulefile_ ...` - Display configuration information about the specified modulefile(s).
*   `module whatis [_modulefile_ ...]` - Display summary information about the specified modulefile(s).
*   `module purge` - Unload all loaded modulefiles.

 Savio uses a hierarchical structure to manage module dependencies, so some modulefiles will only be available after their dependent modulefile is loaded. (For instance, C libraries will only become available after their parent C compiler has been loaded.)

For more detailed usage instructions for the **module** command please run `man module` on the cluster.

### Example Usage:

It can be helpful to try out each of the following examples in sequence to more fully understand how environment modules work. Please note that the output here may vary from your own depending on module updates and your particular access priveliges. 

```shell
[casey@n0000 ~]$ module avail
---- /global/software/sl-7.x86_64/modfiles/langs ----  
clang/3.9.1  cuda/8.0  gcc/4.8.5  gcc/6.3.0  java/1.8.0_121  python/2.7  python/3.6  r/3.4.2
---- /global/software/sl-7.x86_64/modfiles/tools ----  
arpack-ng/3.4.0  gflags/2.2.0  gv/3.7.4  lmdb/0.9.19  nano/2.7.4   qt/5.4.2  texlive/2016
---- /global/software/sl-7.x86_64/modfiles/apps ----  
bio/blast/2.6.0  math/octave/current  ml/mxnet/0.9.3-py35  ml/theano/current-py36 
...
[casey@n0000 ~]$ module list
No Modulefiles Currently Loaded.
[casey@n0000 ~]$ module load intel
[casey@n0000 ~]$ module list
Currently Loaded Modulefiles:  
  1) intel/2016.4.072
[casey@n0000 ~]$ module load openmpi mkl
[casey@n0000 ~]$ module list
Currently Loaded Modulefiles:  
  1) intel/2016.4.072   3) mkl/2016.4.072  
  2) openmpi/2.0.2-intel
[casey@n0000 ~]$ module unload openmpi
[casey@n0000 ~]$ module list
Currently Loaded Modulefiles:  
  1) intel/2016.4.072   2) mkl/2016.4.072
[casey@n0000 ~]$ module switch mkl lapack  
[casey@n0000 ~]$ module list
Currently Loaded Modulefiles:  
  1) intel/2016.4.072   2) lapack/3.8.0-intel
[casey@n0000 ~]$ module show mkl
-------------------------------------------------------------------  
/global/software/sl-7.x86_64/modfiles/intel/2016.4.072/mkl/2016.4.072:

module-whatis    This module sets up MKL 2016.4.072 in your environment.  
setenv           MKL_DIR /global/software/sl-7.x86_64/modules/langs/intel/2016.4.072/mkl  
setenv           MKLROOT /global/software/sl-7.x86_64/modules/langs/intel/2016.4.072/mkl  
prepend-path     CPATH /global/software/sl-7.x86_64/modules/langs/intel/2016.4.072/mkl/include  
prepend-path     CPATH /global/software/sl-7.x86_64/modules/langs/intel/2016.4.072/mkl/include/fftw  
prepend-path     FPATH /global/software/sl-7.x86_64/modules/langs/intel/2016.4.072/mkl/include  
prepend-path     FPATH /global/software/sl-7.x86_64/modules/langs/intel/2016.4.072/mkl/include/fftw  
prepend-path     INCLUDE /global/software/sl-7.x86_64/modules/langs/intel/2016.4.072/mkl/include  
prepend-path     INCLUDE /global/software/sl-7.x86_64/modules/langs/intel/2016.4.072/mkl/include/fftw  
prepend-path     LIBRARY_PATH /global/software/sl-7.x86_64/modules/langs/intel/2016.4.072/mkl/lib/intel64_lin  
prepend-path     LD_LIBRARY_PATH /global/software/sl-7.x86_64/modules/langs/intel/2016.4.072/mkl/lib/intel64_lin  
prepend-path     MIC_LIBRARY_PATH /global/software/sl-7.x86_64/modules/langs/intel/2016.4.072/mkl/lib/intel64_lin_mic  
prepend-path     MIC_LD_LIBRARY_PATH /global/software/sl-7.x86_64/modules/langs/intel/2016.4.072/mkl/lib/intel64_lin_mic  
prepend-path     SINK_LD_LIBRARY_PATH /global/software/sl-7.x86_64/modules/langs/intel/2016.4.072/mkl/lib/intel64_lin_mic  
prepend-path     NLSPATH /global/software/sl-7.x86_64/modules/langs/intel/2016.4.072/mkl/lib/intel64_lin/locale/en_US  
-------------------------------------------------------------------`
[casey@n0000 ~]$ module whatis mkl
mkl                  : This module sets up MKL 2016.4.072 in your environment.
[casey@n0000 ~]$ module purge
[casey@n0000 ~]$ module lis
No Modulefiles Currently Loaded.
[casey@n0000 ~]$ module avail
---- /global/software/sl-7.x86_64/modfiles/langs ----  
clang/3.9.1  cuda/8.0  gcc/4.8.5  gcc/6.3.0  java/1.8.0_121  python/2.7  python/3.6  r/3.4.2
---- /global/software/sl-7.x86_64/modfiles/tools ----  
arpack-ng/3.4.0  gflags/2.2.0  gv/3.7.4  lmdb/0.9.19  nano/2.7.4   qt/5.4.2  texlive/2016
---- /global/software/sl-7.x86_64/modfiles/apps ----  
bio/blast/2.6.0  math/octave/current  ml/mxnet/0.9.3-py35  ml/theano/current-py36 
...
[casey@n0000 ~]$ module load gcc
[casey@n0000 ~]$ module avail
---- /global/software/sl-7.x86_64/modfiles/langs ----  
clang/3.9.1  cuda/8.0  gcc/4.8.5  gcc/6.3.0  java/1.8.0_121  python/2.7  python/3.6  r/3.4.2
---- /global/software/sl-7.x86_64/modfiles/tools ----  
arpack-ng/3.4.0  gflags/2.2.0  gv/3.7.4  lmdb/0.9.19  nano/2.7.4   qt/5.4.2  texlive/2016
---- /global/software/sl-7.x86_64/modfiles/gcc/6.3.0 ----  
antlr/2.7.7-gcc  fftw/2.1.5-gcc  hdf5/1.8.18-gcc-p  ncl/6.3.0-gcc  ncview/2.1.7-gcc  openmpi/2.0.2-gcc
...  
```

**Note how additional modules become available (the final line above) after “_gcc_” is loaded due to the heirarchical structure of module dependencies.** To view a list of all modules including those only visible after loading their dependencies enter the following command:

`find /global/software/sl-7.x86_64/modfiles -type d -exec ls -d {} \;`

## Software Provided on Savio

Research IT provides and maintains a set of broadly used and general purpose software modules. Our aim is to provide an ecosystem that most users can rely on to accomplish their research and studies. 

For a comprehensive and up-to-date list of software provided on the cluster, run the `module avail` command [as described above](#accessing-software-using-environment-modules), or `find /global/software/sl-7.x86_64/modfiles -type d -exec ls -d {} \;` to view an exhaustive list including modules hidden under dependencies.

<table align="center" border="1" cellpadding="0" cellspacing="0">
<thead>
<tr>
<th style="text-align:center">Category</th>
<th style="text-align:center">Application/Library Name</th>
</tr>
</thead>
<tbody>
<tr style="background-color:#D3D3D3">
<td align="center" colspan="2">Development Tools</td>
</tr>
<tr>
<th scope="row">Editor/IDE</th>
<td>Emacs, Vim, Nano, cmake, cscope, ctags</td>
</tr>
<tr>
<th scope="row">SCM</th>
<td>Git, Mercurial</td>
</tr>
<tr>
<th scope="row">Debugger/Profiler/Tracer</th>
<td>GDB, gprof, Valgrind, TAU, Allinea DDT</td>
</tr>
<tr>
<th scope="row">Languages/Platforms</th>
<td>GCC, Intel, Perl, Python, Java, Boost, CUDA, UPC, Open MPI, TBB, MPI4Py, Python / IPython, R, MATLAB, Octave, Julia</td>
</tr>
<tr>
<th scope="row">Math Libraries</th>
<td>MKL, ATLAS, FFTW, FFTW3, GSL, LAPACK, ScaLAPACK, NumPy, SciPy, Eigen</td>
</tr>
<tr>
<th scope="row">IO Libraries</th>
<td>HDF5, NetCDF, NCO, NCL</td>
</tr>
<tr style="background-color:#D3D3D3">
<td align="center" colspan="2">Data processing, Visualization, and Machine Learning Tools</td>
</tr>
<tr>
<th scope="row">Data Processing/Visualization</th>
<td>Gnuplot, Grace, Graphviz, ImageMagick, MATLAB, Octave, ParaView, Python / IPython, R, VisIt, VMD, yt, Matplotlib</td>
</tr>
<tr>
<th scope="row">Machine Learning</th>
<td>Tensorflow, Caffe, H2O, MXNet, Theano, Torch, Scikit-learn</td>
</tr>
<tr>
<th scope="row">Bioinformatics</th>
<td>BLAST, Bowtie, Picard, SAMtools, VCFtools</td>
</tr>
<tr style="background-color:#D3D3D3">
<td align="center" colspan="2">Typesetting and Publishing Tools</td>
</tr>
<tr>
<th scope="row">Typesetting</th>
<td>TeX Live, Ghostscript, Doxygen</td>
</tr>
</tbody>
</table>

## Requesting New Modules or Updates

Software installed within the Environment Module system must meet the requirements specified under ["Installing Software"](installing-software#requirements-for-software-on-savio). Modules are updated at most biannually, and we request that you install and maintain packages that require more frequent updates yourself.

To request additional software be installed or existing packages to be upgraded please use our [Software Request Form](https://docs.google.com/forms/d/e/1FAIpQLSeBR2MsIEgzPq9yvamqYpABBDYZbFccUx85ZjRD1E87s327Ig/viewform?usp=sf_link). Expect to be contacted regarding your request within three business days (except academic holidays).
