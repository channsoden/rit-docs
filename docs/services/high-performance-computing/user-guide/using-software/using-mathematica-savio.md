---
title: Using Mathematica on Savio
keywords: high performance computing, Savio, Mathematica
tags: [hpc, example, script]
---


# Using Mathematica on Savio

This document describes how to use Mathematica on the Savio high-performance computing cluster at the University of California, Berkeley.

## Loading Mathematica

To load Mathematica into your current software environment on Savio, at any shell prompt, enter:
```
module load mathematica
```

To launch the Mathematica graphical user interface (GUI) from a shell prompt on a login node, a compute node, or the visualization node,
enter:
```
mathematica
```

To launch only the Mathematica kernel from a shell prompt, enter:
```
math
```
## Licensing

The <a href="https://software.berkeley.edu/mathematica"
target="_blank">UC Berkeley Mathematica license</a> makes Mathematica available at no direct cost to all Savio users.

## Running Mathematica notebooks in SLURM batch jobs

To submit Mathematica jobs to Savio compute nodes via a SLURM batch script, the Mathematica commands to be executed must be contained in a single .m script. The .m script will then be passed to the `math` command in an sbatch script. 

For more detailed information on using `sbatch` and job
script files, see [Running Your Jobs](../../../../../services/high-performance-computing/user-guide/running-your-jobs).

As an example, here is a basic Mathematica script called `math-simple.m` that computes the sum of A and B:

```
A = Sum[i, {i,1,100}]
B = Mean[{25, 36, 22, 16, 8, 42}]
Answer = A + B
Quit[];
```

This script can be submited to SLURM with an sbatch SLURM shell script (e.g.,`sbatch math-simple.sh`), which will send the job to a compute node:

```
#!/bin/bash
# Job name:
#SBATCH --job-name=mathematica_example1
#
# Account:
#SBATCH --account=account_name
#
# Partition:
#SBATCH --partition=partition_name
#
# Number of tasks per node:
#SBATCH --ntasks=1
#
# Wall clock limit:
#SBATCH --time=00:5:00
#
## Commands to run:
module load mathematica/11.1
math -run < math-simple.m
```

and the following output is obtained when the job completes:

```
In[1]:= 
Out[1]= 5050

In[2]:= 
        149
Out[2]= ---
         6

In[3]:= 
        30449
Out[3]= -----
          6
```

Of course if one uses a partition in which jobs are allocated a full
node, this job will still be charged for all the cores on the node
even when only one core is used. 

## Using Multiple CPUs in Mathematica

Mathematica can be run in parallel using the built in `Parallel`
commands or by utilizing  the parallel API. Parallel Mathematica jobs
are limited to one node, but can utilize all CPU cores on the
node. Here we request and use eight cores (but would have access to all the cores
on the node):


```
#!/bin/bash
# Job name:
#SBATCH --job-name=mathematica_example2
#
# Account:
#SBATCH --account=account_name
#
# Partition:
#SBATCH --partition=partition_name
#
# Request one node:
#SBATCH --ntasks=1
#
# Number of tasks per node:
#SBATCH --ncpus-per-task=8
#
# Wall clock limit:
#SBATCH --time=00:10:00
#
## Commands to run:
module load mathematica/11.1
math -run < ./sample-parallel.m
```

and the example parallel Mathematica script `sample-parallel.m` in this case is:

```
(*Limits Mathematica to requested resources*)
Unprotect[$ProcessorCount];$ProcessorCount = 8;
 
(*Prints the machine name that each kernel is running on*)
Print[ParallelEvaluate[$MachineName]];

(*Prints all Mersenne Prime numbers less than 2000*)
Print[Parallelize[Select[Range[2000],PrimeQ[2^#-1]&]]];
```

The output obtained when the job completes includes:

```
In[1]:= 
In[2]:= 
In[3]:= 
In[4]:= 
In[5]:= Launching kernels...
Updating from Wolfram Research server ...

{n0040, n0040, n0040, n0040, n0040, n0040, n0040, n0040}

In[6]:= In[6]:= 
In[7]:= {2, 3, 5, 7, 13, 17, 19, 31, 61, 89, 107, 127, 521, 607, 1279}

In[8]:= 
```
