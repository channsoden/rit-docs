---
title: Using Software on Savio
keywords: high performance computing, berkeley research computing
tags: [hpc]
---

<p>This page provides links to introductory guides on the use of various software applications, on the Savio high performance computing cluster at the University of California, Berkeley.</p>
<ul><li><strong><a href="../../../../services/high-performance-computing/user-guide/using-software/using-python-savio">Using Python on Savio</a></strong><br>
An introduction to using <a href="https://www.python.org" target="_blank">Python</a>, a programming language widely used in scientific computing, on Savio.</li>
<li><a href="../../../../services/high-performance-computing/user-guide/using-software/using-r-savio"><strong>Using R on Savio</strong></a><br>
An introduction to using <a href="https://www.r-project.org/" target="_blank">R</a>, a language and environment for statistical computing and graphics, on Savio.</li>
<li><strong><a href="../../../../services/high-performance-computing/user-guide/using-software/using-matlab-savio">Using MATLAB on Savio</a></strong><br>
An introduction to using&nbsp;<a href="https://www.mathworks.com/products/matlab.html" target="_blank">MATLAB</a>, a matrix-based, technical computing language and environment for solving engineering and scientific problems, on Savio.</li>
<li><strong><a href="../../../../services/high-performance-computing/user-guide/using-software/using-mathematica-savio">Using Mathematica on Savio</a></strong><br>
An introduction to using&nbsp;<a href="https://www.wolfram.com/mathematica/" target="_blank">Mathematica</a> on Savio.</li>
<li><a href="../../../../services/high-performance-computing/user-guide/using-software/using-singularity-savio"><strong>Using Singularity on Savio</strong></a><br>
An introduction to using <a href="http://singularity.lbl.gov/" target="_blank">Singularity</a>, a software tool that facilitates the movement of software applications and workflows between various computational environments, on Savio.</li>
<li><a href="../../../../services/high-performance-computing/user-guide/using-software/using-hadoop-and-spark-savio"><strong>Using Hadoop and Spark on Savio</strong></a><br>
An introduction and guides to using the <a href="http://hadoop.apache.org" target="_blank">Hadoop</a> and <a href="http://spark.apache.org" target="_blank">Spark</a> frameworks via auxiliary scripts on Savio.</li>
</ul><p>Additional introductory guides for using C compilers and other software applications are in planning or development. If you have any interest in working on or testing one of these, or have suggestions for other such guides, please contact us via our <a href="../../../../services/high-performance-computing/getting-help">Getting Help</a> email address!</p>
<p>For information on which software applications are provided on the Savio cluster, and on installing your own, additional software on the cluster, please see <a href="../../../../services/high-performance-computing/user-guide/accessing-software">Software Available on Savio</a> and <a href="../../../../services/high-performance-computing/user-guide/installing-software">Accessing and Installing Software</a>.</p>
