---
title: Home
keywords: welcome 
tags: [docs]
---

# Research IT Technical Documentation

Welcome to the Research IT technical documentation website.

## RIT Service Areas

* [High Performance Computing](services/high-performance-computing/)
* [Research Data Management](https://research-it.berkeley.edu/services/research-data-management-service)
* [Analytics Environments on Demand](https://research-it.berkeley.edu/services/analytics-environments-demand)
* [Cloud Computing Support](https://research-it.berkeley.edu/services/cloud-computing-support)

## Community Contributions

This site is hosted on [Gitlab](https://gitlab.com/ucb-rit/rit-docs). See our [Community Contributions Guide](contributing) to learn more.
