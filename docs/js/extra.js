// Custom JS

let research_data_link = "https://research-it.berkeley.edu/services/research-data-management-service";
let aeod_link = "https://research-it.berkeley.edu/services/analytics-environments-demand";
let cloud_link = "https://research-it.berkeley.edu/services/cloud-computing-support";

/* FAQ Page: 
    Make all list items with class "question" behave as accordion dropdowns.

    Steps: 
        1. On click event fire 
        2. Get array of all divs class="question"
        3. Fn: Close all questions by removing attribute "open"
        4. Fn: Add attribute "open" to div that is currently clicked 
*/

let qCollection = document.getElementsByClassName("question");

let qArray = Array.from(qCollection);

function closeAllQuestions(questionArray) {
    questionArray.forEach(q => q.removeAttribute("open"));
}

function addOpenAttribute(questionClicked) {
    questionClicked.addAttribute("open");
}

